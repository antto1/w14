import React , { useState}  from 'react';
import './App.css';

function App() {
  const [chatInput, setChatInput] = useState('');
  
  function showMessages() {
    const chatsString = localStorage.getItem('chats');
    if (chatsString) {
      const chats = JSON.parse(chatsString);
      console.log('Chats:', chats);
    } else {
      console.log('No chats available.');
    }
  }

  function sendMessages() {
    const chat = {
      message: chatInput,
      timestamp: new Date().toLocaleString(),
    };

    const chatsString = localStorage.getItem('chats');
    const chats = chatsString ? JSON.parse(chatsString) : [];

    chats.push(chat);

    localStorage.setItem('chats', JSON.stringify(chats));

    setChatInput('');
  }

  return (
    <div className="App">
      <input
        type="text"
        id="example"
        value={chatInput}
        onChange={(e) => setChatInput(e.target.value)}
      />
      <button onClick={sendMessages}>Send</button>
      <br></br>
      <button onClick={showMessages}>Show messages</button>
    </div>
  );
}

export default App;
